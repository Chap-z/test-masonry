(function($){
    
    // $('.owl-carousel').owlCarousel({
    //     loop:true,
    //     autoplay: true,
    //     autoplayTimeout:7000,
    //     autoplaySpeed: 1500,
    //     items:1
    // })


    jQuery("#btn_menu").click(function(event){
        event.preventDefault ? event.preventDefault() : event.returnValue = false;
        if(jQuery("#deroulant").hasClass("actif")){
            jQuery("#deroulant").removeClass("actif");
            jQuery("#btn_menu").removeClass("actif");
        }else{
            jQuery("#deroulant").addClass("actif");
            jQuery("#btn_menu").addClass("actif");
        }
    })

    // $('.row-img-content .wrapper').click(function(){
    //     var img = $(this).html();
    //     var imgToReplace = $(this).parent().parent().find('.img-principale').html();
    //     $(this).parent().parent().find('.img-principale').html(img);
    //     $(this).html(imgToReplace);
    // });

    
// external js: masonry.pkgd.js

$('.grid').masonry({
  itemSelector: '.grid-item',
  columnWidth: 20
});
            
    
  //Listing product page
$(function () {
    var selectedClass = "";
    $(".fil-cat").click(function () {
      $('.grid > div').addClass('grid-item');
      $('.fil-cat').removeClass("active");
      $(this).addClass("active");
      selectedClass = $(this).attr("data-rel");
   
      $("#grid").fadeTo(100, 0.1);
      $(".grid-item").not("." + selectedClass).fadeOut().removeClass('scale-anm').removeClass('grid-item');
      setTimeout(function () {
        $("." + selectedClass).fadeIn().addClass('scale-anm');
        $("#grid").fadeTo(300, 1);
        $('.grid').masonry( 'reloadItems' );
      }, 300);
      $('.grid').masonry( 'reloadItems' );
    });
  });

})(jQuery);


